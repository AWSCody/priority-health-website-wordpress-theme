<?php
/*
 Template Name: Commitment Template
 */

include dirname(__FILE__) . "/common.php";



$data["TOP_TITLE"] = get_post_meta($original_post_id, 'TOP_TITLE', true);
$data["TOP_CAPTION"] = get_post_meta($original_post_id, 'TOP_CAPTION', true);
$data["TOP_CONTENT"] = get_post_meta($original_post_id, 'TOP_CONTENT', true);

$data["BOTTOM_TITLE"] = get_post_meta($original_post_id, 'BOTTOM_TITLE', true);
$data["BOTTOM_CAPTION"] = get_post_meta($original_post_id, 'BOTTOM_CAPTION', true);
$data["BOTTOM_CONTENT"] = get_post_meta($original_post_id, 'BOTTOM_CONTENT', true);
$data["BOTTOM_IMAGE"] = get_post_meta($original_post_id, 'BOTTOM_IMAGE', true);















$data["PAGE"]="Pages/Commitment.html";
$common -> setDataArray($data);
$data = $common -> compile();

$data["bootstrap_required"]=true;


Display_Component::renderDisplay(dirname(__FILE__) . "/Templates", "Site.html", $data);
?>