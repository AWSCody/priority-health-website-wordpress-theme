<?php
/*
 Template Name: Plain Layout
 */

include dirname(__FILE__) . "/common.php";

$data["STYLE"] = get_post_meta($post -> ID, 'style', true);
$data["JAVASCRIPT"] = get_post_meta($post -> ID, 'javascript', true);


$common -> setDataArray($data);
$data = $common -> compile();

Display_Component::renderDisplay(dirname(__FILE__) . "/Templates/Pages/", "Plain.html", $data);
?>