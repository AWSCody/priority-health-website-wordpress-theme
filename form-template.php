<?php
/*
 Template Name: Form Template
 */

include dirname(__FILE__) . "/common.php";

$data["LEFT_TITLE"] = get_post_meta($original_post_id, 'LEFT_TITLE', true);
$data["LEFT_CAPTION"] = get_post_meta($original_post_id, 'LEFT_CAPTION', true);

$data["RIGHT_TITLE"] = get_post_meta($original_post_id, 'RIGHT_TITLE', true);
$data["RIGHT_CAPTION"] = get_post_meta($original_post_id, 'RIGHT_CAPTION', true);

$data["ONLINE_REFERRAL_TITLE"] = get_post_meta($original_post_id, 'ONLINE_REFERRAL_TITLE', true);
$data["ONLINE_FORM"] = get_post_meta($original_post_id, 'ONLINE_FORM', true);

$data["FORM_1_IMAGE"] = get_post_meta($original_post_id, 'FORM_1_IMAGE', true);
$data["FORM_2_IMAGE"] = get_post_meta($original_post_id, 'FORM_2_IMAGE', true);

$data["DOWNLOAD_FORM"] = get_post_meta($original_post_id, 'DOWNLOAD_FORM', true);

$data["POST_OFFICE_TITLE"] = get_post_meta($original_post_id, 'POST_OFFICE_TITLE', true);
$data["POST_OFFICE_INFORMATION"] = get_post_meta($original_post_id, 'POST_OFFICE_INFORMATION', true);

$data["FAX_TITLE"] = get_post_meta($original_post_id, 'FAX_TITLE', true);
$data["FAX_NUMBER"] = get_post_meta($original_post_id, 'FAX_NUMBER', true);
















$data["CONTENT_PAGE"]="Pages/Forms/Infant_Risk.html";

$data["PAGE"]="Pages/Forms.html";
$common -> setDataArray($data);
$data = $common -> compile();

$data["bootstrap_required"]=true;


Display_Component::renderDisplay(dirname(__FILE__) . "/Templates", "Site.html", $data);
?>