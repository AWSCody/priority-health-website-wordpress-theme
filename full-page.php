<?php
/*
 Template Name: Full Page Template
 */

include dirname(__FILE__) . "/common.php";

$data["CONTENT_TITLE"] = get_post_meta($original_post_id, 'CONTENT_TITLE', true);
$data["CONTENT_CAPTION"] = get_post_meta($original_post_id, 'CONTENT_CAPTION', true);

$data["MAIN_TITLE"] = get_post_meta($original_post_id, 'MAIN_TITLE', true);
$data["MAIN_IMAGE"] = get_post_meta($original_post_id, 'MAIN_IMAGE', true);
$data["MAIN_CONTENT"] = get_post_meta($original_post_id, 'MAIN_CONTENT', true);




$data["COLUMN_1"] = get_post_meta($postid, 'COLUMN_1', true);
$data["COLUMN_1_TITLE"] = get_post_meta($postid, 'COLUMN_1_TITLE', true);
$data["COLUMN_1_CONTENT"] = get_post_meta($postid, 'COLUMN_1_CONTENT', true);

$data["COLUMN_2"] = get_post_meta($postid, 'COLUMN_2', true);
$data["COLUMN_2_TITLE"] = get_post_meta($postid, 'COLUMN_2_TITLE', true);
$data["COLUMN_2_CONTENT"] = get_post_meta($postid, 'COLUMN_2_CONTENT', true);

$data["COLUMN_3"] = get_post_meta($postid, 'COLUMN_3', true);
$data["COLUMN_3_TITLE"] = get_post_meta($postid, 'COLUMN_3_TITLE', true);
$data["COLUMN_3_CONTENT"] = get_post_meta($postid, 'COLUMN_3_CONTENT', true);

$data["MIDDLE_TITLE"] = get_post_meta($postid, 'MIDDLE_TITLE', true);
$data["MIDDLE_CONTENT"] = get_post_meta($postid, 'MIDDLE_CONTENT', true);

$data["TEAM_TITLE"] = get_post_meta($postid, 'TEAM_TITLE', true);

$data["MEMBER_1_IMAGE"] = get_post_meta($postid, 'MEMBER_1_IMAGE', true);
$data["MEMBER_1_NAME"] = get_post_meta($postid, 'MEMBER_1_NAME', true);
$data["MEMBER_1_TITLE"] = get_post_meta($postid, 'MEMBER_1_TITLE', true);

$data["MEMBER_2_IMAGE"] = get_post_meta($postid, 'MEMBER_2_IMAGE', true);
$data["MEMBER_2_NAME"] = get_post_meta($postid, 'MEMBER_2_NAME', true);
$data["MEMBER_2_TITLE"] = get_post_meta($postid, 'MEMBER_2_TITLE', true);

$data["MEMBER_3_IMAGE"] = get_post_meta($postid, 'MEMBER_3_IMAGE', true);
$data["MEMBER_3_NAME"] = get_post_meta($postid, 'MEMBER_3_NAME', true);
$data["MEMBER_3_TITLE"] = get_post_meta($postid, 'MEMBER_3_TITLE', true);

$data["MEMBER_4_IMAGE"] = get_post_meta($postid, 'MEMBER_4_IMAGE', true);
$data["MEMBER_4_NAME"] = get_post_meta($postid, 'MEMBER_4_NAME', true);
$data["MEMBER_4_TITLE"] = get_post_meta($postid, 'MEMBER_4_TITLE', true);









$data["PAGE"]="Pages/About.html";
$common -> setDataArray($data);
$data = $common -> compile();

$data["bootstrap_required"]=true;


Display_Component::renderDisplay(dirname(__FILE__) . "/Templates", "Site.html", $data);
?>