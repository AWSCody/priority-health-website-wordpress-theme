<?php
/*
 Template Name: Hero-Display Template
 */

include dirname(__FILE__) . "/common.php";

$data["ABOUT_US"] = get_post_meta($postid, 'about_us', true);
$data["SERVICES_LIST"] = get_post_meta($postid, 'services_list', true);

$data["PROMO_1"] = get_post_meta($postid, 'promo_1', true);
$data["PROMO_1_TITLE"] = get_post_meta($postid, 'PROMO_1_TITLE', true);
$data["PROMO_1_CAPTION"] = get_post_meta($postid, 'PROMO_1_CAPTION', true);
$data["PROMO_1_CONTENT"] = get_post_meta($postid, 'PROMO_1_CONTENT', true);
$data["PROMO_1_IMAGE"] = get_post_meta($postid, 'PROMO_1_IMAGE', true);

$data["PROMO_2"] = get_post_meta($postid, 'promo_2', true);
$data["PROMO_2_TITLE"] = get_post_meta($postid, 'PROMO_2_TITLE', true);
$data["PROMO_2_CAPTION"] = get_post_meta($postid, 'PROMO_2_CAPTION', true);
$data["PROMO_2_CONTENT"] = get_post_meta($postid, 'PROMO_2_CONTENT', true);
$data["PROMO_2_IMAGE"] = get_post_meta($postid, 'PROMO_2_IMAGE', true);



$data["PROMO_3"] = get_post_meta($postid, 'promo_3', true);
$data["PROMO_3_TITLE"] = get_post_meta($postid, 'PROMO_3_TITLE', true);
$data["PROMO_3_CAPTION"] = get_post_meta($postid, 'PROMO_3_CAPTION', true);
$data["PROMO_3_CONTENT"] = get_post_meta($postid, 'PROMO_3_CONTENT', true);
$data["PROMO_3_IMAGE"] = get_post_meta($postid, 'PROMO_3_IMAGE', true);

$data["COLUMN_1"] = get_post_meta($postid, 'COLUMN_1', true);
$data["COLUMN_1_TITLE"] = get_post_meta($postid, 'COLUMN_1_TITLE', true);
$data["COLUMN_1_CONTENT"] = get_post_meta($postid, 'COLUMN_1_CONTENT', true);

$data["COLUMN_2"] = get_post_meta($postid, 'COLUMN_2', true);
$data["COLUMN_2_TITLE"] = get_post_meta($postid, 'COLUMN_2_TITLE', true);
$data["COLUMN_2_CAPTION"] = get_post_meta($postid, 'COLUMN_2_CAPTION', true);
$data["COLUMN_2_CONTENT"] = get_post_meta($postid, 'COLUMN_2_CONTENT', true);


$data["COLUMN_3"] = get_post_meta($postid, 'COLUMN_3', true);
$data["COLUMN_3_TITLE"] = get_post_meta($postid, 'COLUMN_3_TITLE', true);
$data["COLUMN_3_IMAGE"] = get_post_meta($postid, 'COLUMN_3_IMAGE', true);
$data["COLUMN_3_CONTENT"] = get_post_meta($postid, 'COLUMN_3_CONTENT', true);

$data["RIGHT_CONTENT"] = get_post_meta($postid, 'RIGHT_CONTENT', true);

$data["TOP_1"] = get_post_meta($postid, 'TOP_1', true);
$data["TOP_1_IMAGE"] = get_post_meta($postid, 'TOP_1_IMAGE', true);
$data["TOP_1_TITLE"] = get_post_meta($postid, 'TOP_1_TITLE', true);
$data["TOP_1_CONTENT"] = get_post_meta($postid, 'TOP_1_CONTENT', true);

$data["TOP_2"] = get_post_meta($postid, 'TOP_2', true);
$data["TOP_2_IMAGE"] = get_post_meta($postid, 'TOP_2_IMAGE', true);
$data["TOP_2_TITLE"] = get_post_meta($postid, 'TOP_2_TITLE', true);
$data["TOP_2_CONTENT"] = get_post_meta($postid, 'TOP_2_CONTENT', true);

$data["TOP_3"] = get_post_meta($postid, 'TOP_3', true);
$data["TOP_3_IMAGE"] = get_post_meta($postid, 'TOP_3_IMAGE', true);
$data["TOP_3_TITLE"] = get_post_meta($postid, 'TOP_3_TITLE', true);
$data["TOP_3_CONTENT"] = get_post_meta($postid, 'TOP_3_CONTENT', true);

$data["TOP_4"] = get_post_meta($postid, 'TOP_4', true);
$data["TOP_4_IMAGE"] = get_post_meta($postid, 'TOP_4_IMAGE', true);
$data["TOP_4_TITLE"] = get_post_meta($postid, 'TOP_4_TITLE', true);
$data["TOP_4_CONTENT"] = get_post_meta($postid, 'TOP_4_CONTENT', true);

$data["TOP_5"] = get_post_meta($postid, 'TOP_5', true);
$data["TOP_5_IMAGE"] = get_post_meta($postid, 'TOP_5_IMAGE', true);
$data["TOP_5_TITLE"] = get_post_meta($postid, 'TOP_5_TITLE', true);
$data["TOP_5_CONTENT"] = get_post_meta($postid, 'TOP_5_CONTENT', true);
















$data["SLIDESHOW_IMAGES"] = get_post_meta($postid, 'slideshow_images', true);
$data["HEADLINE_TEXT"] = get_post_meta($postid, 'page_headline_text', true);


$data["PAGE"]="Pages/Main.html";



$common -> setDataArray($data);
$data=$common -> compile();

$data["bootstrap_required"]=true;

Display_Component::renderDisplay(dirname(__FILE__) . "/Templates", "Site.html", $data);
?>