<?php
/*
 Template Name: Maternal Form Template
 */

include dirname(__FILE__) . "/common.php";

$data["MAIN_TITLE"] = get_post_meta($original_post_id, 'MAIN_TITLE', true);
$data["LAST_UPDATED"] = get_post_meta($original_post_id, 'LAST_UPDATED', true);

$data["TITLE_1"] = get_post_meta($original_post_id, 'TITLE_1', true);
$data["FORM_SECTION_1"] = get_post_meta($original_post_id, 'FORM_SECTION_1', true);

$data["TITLE_2"] = get_post_meta($original_post_id, 'TITLE_2', true);
$data["FORM_SECTION_2"] = get_post_meta($original_post_id, 'FORM_SECTION_2', true);

$data["TITLE_3"] = get_post_meta($original_post_id, 'TITLE_3', true);
$data["FORM_SECTION_3"] = get_post_meta($original_post_id, 'FORM_SECTION_3', true);

$data["TITLE_4"] = get_post_meta($original_post_id, 'TITLE_4', true);
$data["FORM_SECTION_4"] = get_post_meta($original_post_id, 'FORM_SECTION_4', true);

$data["TITLE_5"] = get_post_meta($original_post_id, 'TITLE_5', true);
$data["FORM_SECTION_5"] = get_post_meta($original_post_id, 'FORM_SECTION_5', true);

$data["TITLE_6"] = get_post_meta($original_post_id, 'TITLE_6', true);
$data["FORM_SECTION_6"] = get_post_meta($original_post_id, 'FORM_SECTION_6', true);

$data["TITLE_7"] = get_post_meta($original_post_id, 'TITLE_7', true);
$data["FORM_SECTION_7"] = get_post_meta($original_post_id, 'FORM_SECTION_7', true);

$data["TITLE_8"] = get_post_meta($original_post_id, 'TITLE_8', true);
$data["FORM_SECTION_8"] = get_post_meta($original_post_id, 'FORM_SECTION_8', true);

$data["TITLE_9"] = get_post_meta($original_post_id, 'TITLE_9', true);
$data["FORM_SECTION_9"] = get_post_meta($original_post_id, 'FORM_SECTION_9', true);

$data["TITLE_10"] = get_post_meta($original_post_id, 'TITLE_10', true);
$data["FORM_SECTION_10"] = get_post_meta($original_post_id, 'FORM_SECTION_10', true);

$data["TITLE_11"] = get_post_meta($original_post_id, 'TITLE_11', true);
$data["FORM_SECTION_11"] = get_post_meta($original_post_id, 'FORM_SECTION_11', true);

$data["TITLE_12"] = get_post_meta($original_post_id, 'TITLE_12', true);
$data["FORM_SECTION_12"] = get_post_meta($original_post_id, 'FORM_SECTION_12', true);
















$data["PAGE"]="Pages/Forms/Maternal_Risk.html";
$common -> setDataArray($data);
$data = $common -> compile();

$data["bootstrap_required"]=true;


Display_Component::renderDisplay(dirname(__FILE__) . "/Templates", "Site.html", $data);
?>